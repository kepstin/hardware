# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=dispcalgui suffix=tar.gz ] \
    setup-py [ import=distutils blacklist="3" ] \
    udev-rules [ udev_files=[ misc/55-Argyll.rules ] ] \
    freedesktop-desktop gtk-icon-cache

SUMMARY="Open Source Display Calibration and Characterization"
DESCRIPTION="
Calibrates and characterizes display devices using a hardware sensor,
driven by the open source color management system Argyll CMS.
Supports multi-display setups and a variety of available settings like
customizable whitepoint, luminance, black level, tone response curve
as well as the creation of matrix and look-up-table ICC profiles with
optional gamut mapping. Calibrations and profiles can be verified
through measurements, and profiles can be installed to make them
available to color management aware applications.
Profile installation can utilize Argyll CMS, Oyranos and/or GNOME
Color Manager if available, for flexible integration.
"
HOMEPAGE+=" https://displaycal.net"

BUGS_TO="pyromaniac@thwitt.de"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/wxPython:=[>=2.8.11][python_abis:*(-)?]
    run:
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        measurement/argyllcms
        media-libs/SDL_mixer:2
"

SETUP_PY_SRC_INSTALL_PARAMS=( --install-data /usr/share )

install_one_multibuild() {
    local old_XDG="${XDG_DATA_DIRS}"

    # We have to create these directories before installing because xdg-* needs
    # to see them even if it doesn't use them...
    dodir \
        /etc/xdg/menus \
        /usr/share/applications \
        /usr/share/applnk \
        /usr/share/desktop-directories \
        /usr/share/icons/hicolor \
        /usr/share/mime/packages \
        ${py_site_dir/${prefix}}

    export \
        XDG_DATA_DIRS="${IMAGE}"/usr/share \
        XDG_UTILS_INSTALL_MODE="system" \

    setup-py_install_one_multibuild

    export XDG_DATA_DIRS="${old_XDG}"

    nonfatal edo rm "${IMAGE}"/usr/share/applications/mimeinfo.cache

    edo rm -r "${IMAGE}"/etc/udev
    install_udev_files

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

